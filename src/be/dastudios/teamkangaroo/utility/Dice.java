package be.dastudios.teamkangaroo.utility;

import java.util.Random;

public class Dice {
    private static int value;

    public static int roll(int sides){
        Random random =new Random();
        value= random.nextInt(sides)+1;
        return value;
    }
}
