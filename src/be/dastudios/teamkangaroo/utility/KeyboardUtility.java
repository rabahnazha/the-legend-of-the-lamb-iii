package be.dastudios.teamkangaroo.utility;


import be.dastudios.teamkangaroo.entity.Gender;

import java.util.Scanner;

import static be.dastudios.teamkangaroo.constant.AppConstants.*;

public abstract class KeyboardUtility {
    private static final String INVALID_MSG = "Invalid input! Please try again...";
    public static final Scanner KEYBOARD = new Scanner(System.in);

    public static int askForInt(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }


    public static boolean askYOrN(String message) {
        while (true) {
            String input = ask(message + "(y/n)");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
                continue;
            }
            switch (firstLetter) {
                case 'y':
                    return true;
                case 'n':
                    return false;
                default:
                    break;
            }
        }
    }

    public static String ask(String message) {
        System.out.println(message);
        return KEYBOARD.nextLine();
    }
    public static String askForName() {
        System.out.println(askForNameOfAdventurer);
        String name= KEYBOARD.nextLine();
        String nameText =  name + afterNameText;
        System.out.println(nameText);
        return name;
    }
    public static String askForGender(String adventurersName) {
        System.out.printf(askForChooseGender, adventurersName);
        while (true) {
            String genderString = KEYBOARD.nextLine();
            if (!genderString.toUpperCase().equals("MALE") && !genderString.toUpperCase().equals("FEMALE")) {
                System.out.println(INVALID_MSG);
            } else return genderString;
        }
    }
    public static String chooseForRace() {
        System.out.println(askForRace);
        while (true) {
            String raceString = KEYBOARD.nextLine();
            if (!raceString.toUpperCase().equals("HUMAN") && !raceString.toUpperCase().equals("ELF") && !raceString.toUpperCase().equals("DWARF")) {
                System.out.println(INVALID_MSG);
            } else return raceString;
        }
    }
    public static String chooseForProfession() {
        System.out.println(askForProfession);
        while (true) {
            String professionString = KEYBOARD.nextLine();
            if (!professionString.toUpperCase().equals("MAGE") && !professionString.toUpperCase().equals("RANGER")
                    && !professionString.toUpperCase().equals("ROGUE") && !professionString.toUpperCase().equals("WARRIOR")) {
                System.out.println(INVALID_MSG);
            } else return professionString.toLowerCase();
        }
    }

    public static int askForChoice(String message, String[] options) {
        System.out.println(message);
        return askForChoice(options);
    }

    public static int askForChoice(String[] options) {
        while (true) {
            for (int i = 0; i < options.length; i++) {
                System.out.printf("%d. %s%n", i + 1, options[i]);
            }
            int chosenIdx = askForInt(String.format("Enter your choice (1-%d):", options.length)) - 1;
            if (chosenIdx < 0 || chosenIdx >= options.length){
                System.out.println(INVALID_MSG);
                System.out.println("Please enter a choice in the valid range");
            } else {
                return chosenIdx;
            }
        }
    }

}


