package be.dastudios.teamkangaroo.repository;

import be.dastudios.teamkangaroo.entity.Adventurer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created By Moon
 * 2/26/2021, Fri
 **/
public class GameRepoImpl implements GameRepo{
    public static final Path PATH_TO_DESKTOP = Path.of(System.getProperty("user.home"),"Masaüstü");
    public static final Path PATH_TO_LEGEND = Path.of("LEGEND");
    public static final Path ABSOLUTE_PATH_TO_LEGEND = PATH_TO_DESKTOP.resolve(PATH_TO_LEGEND);

    private int[] locationInfo;
   private Adventurer adventurer = new Adventurer();

    @Override
    public String[] findAll() {
        File folder = new File(ABSOLUTE_PATH_TO_LEGEND.toString());
        File[] listOfFiles = folder.listFiles();

        try {
            String[] fileNames = new String[listOfFiles.length];

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    fileNames[i]= listOfFiles[i].getName();
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println("Directory " + listOfFiles[i].getName());
                }
            }
            return fileNames;
        } catch (Exception e) {
            System.out.println("There is no saved game in your list.");
        }
        return null;


    }

    @Override
    public void save(Adventurer adventurer, String nameOfSavedGame) {
        this.adventurer = adventurer;

        File gameFolder = new File(PATH_TO_DESKTOP + "\\LEGEND");

        if (!gameFolder.exists()){
            gameFolder.mkdirs();
        }
        writeAdventurerToLegendFolder(nameOfSavedGame);
    }

    @Override
    public Adventurer getByFileName(String chosenFileName) {

        Path chosen = Path.of(chosenFileName);
         ABSOLUTE_PATH_TO_LEGEND.resolve(chosen);

        try(
                FileInputStream fileInput = new FileInputStream(ABSOLUTE_PATH_TO_LEGEND.resolve(chosen).toFile());
                ObjectInputStream objectInput = new ObjectInputStream(fileInput);
        ){

            Adventurer adventurer = (Adventurer) objectInput.readObject();
            return adventurer;

        } catch (IOException | ClassNotFoundException exception){
            System.out.println("DB ERROR...");
        }
       return null;
    }

    private void writeAdventurerToLegendFolder(String nameOfSavedGame) {

        LocalDate currentDate = LocalDate.now();
        try(
                FileOutputStream fileOutput = new FileOutputStream(ABSOLUTE_PATH_TO_LEGEND.resolve(nameOfSavedGame + " " + currentDate +".txt").toFile());
                ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
        ){
            objectOutput.writeObject(adventurer);
        } catch(IOException ioException){
            ioException.printStackTrace();
        }
    }
    @Override
    public void deleteAll() {
        try {
            Files.walk(ABSOLUTE_PATH_TO_LEGEND)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);

        } catch (IOException e) {
            System.out.println("There is nothing to delete");
        }
    }
}
