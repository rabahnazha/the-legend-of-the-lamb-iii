package be.dastudios.teamkangaroo.repository;

import be.dastudios.teamkangaroo.entity.Adventurer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Moon
 * 2/26/2021, Fri
 **/
public interface GameRepo {
    String[] findAll();

    void save(Adventurer adventurer, String nameOfSavedGame);

    Adventurer getByFileName(String chosenFileName);
    void deleteAll();
}
