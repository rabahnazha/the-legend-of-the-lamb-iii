package be.dastudios.teamkangaroo.service;

import be.dastudios.teamkangaroo.entity.Adventurer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Moon
 * 2/26/2021, Fri
 **/
public interface FileService {
    String[] getAllSavedGamesNames();

    void createGameFolder(Adventurer adventurer);

    Adventurer getByName(String chosenFileName);

    void resetAllSavedGame();
}
