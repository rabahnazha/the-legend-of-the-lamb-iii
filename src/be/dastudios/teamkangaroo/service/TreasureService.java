package be.dastudios.teamkangaroo.service;

import be.dastudios.teamkangaroo.entity.Adventurer;

/**
 * Created By Moon
 * 2/25/2021, Thu
 **/
public interface TreasureService {
    void openTreasure(Adventurer adventurer);
}
