package be.dastudios.teamkangaroo.service.impl;

import be.dastudios.teamkangaroo.service.MapService;


public class MapServiceImpl implements MapService {

    public String[][] map1= new String[15][15];
    private String[][] map2= new String[15][15];

    private static final String map1Name = "Fields of Generix";
    private static final String map2Name = "Cave of Thread";

    public  String[][] createMap2() {
        int[][] monsterIndexMap2 = {{0, 12}, {1, 5}, {2, 3}, {3, 9}, {5, 1}, {5, 6}, {4, 13}, {9, 7}, {12, 3}, {12, 11}, {12, 13}, {11, 6}, {14, 1}, {14, 6}};
        int[][] keyIndexMap2 = {{5, 9}, {8, 2}, {9, 6}};
        int[][] magicHouseIndexMap2 = {{2, 1}, {8, 8}, {8, 13}, {13, 10}};
        int[][] wolfIndexMap2 = {{0, 8}, {4, 0}, {10, 8}, {11, 4}, {14, 14}};

        int[][] riverIndexMap2 = {{0, 14}, {1, 14}, {1, 13}, {2, 13}, {2, 12}, {2, 11}, {3, 11}, {4, 11}, {5, 11}, {6, 11}, {6, 10}, {6, 9}, {6, 8},
                {6, 7}, {6, 5}, {6, 4}, {7, 4}, {8, 4}, {9, 4}, {9, 3}, {9, 2}, {10, 2}, {11, 2}, {12, 2}, {12, 1}, {12, 0}};

        for (int[] ints : monsterIndexMap2) {
            map2[ints[0]][ints[1]] = "Monster. It is maybe a strong Enemy.";
        }
        for (int[] ints : keyIndexMap2) {
            map2[ints[0]][ints[1]] = "Treasure. You can open and choose new Items. ";
        }
        for (int[] ints : magicHouseIndexMap2) {
            map2[ints[0]][ints[1]] = "Magic House. You can find some food and drink to raise your Health Points and Stamina Points";
        }
        for (int[] ints : wolfIndexMap2) {
            map2[ints[0]][ints[1]] = "Wild Animals. Big Challenge.";
        }
        for (int[] ints : riverIndexMap2) {
            map2[ints[0]][ints[1]] = "River, it is impossible to go. ";
        }
        return map2;
    }


    public  String[][] createMap1() {
        int[][] monsterIndexMap1 = {{1, 12}, {2, 3}, {3, 9}, {5, 1}, {5, 5}, {6, 8}, {6, 11}, {8, 5}, {9, 1}, {11, 6}, {12, 9}, {12, 13}, {13, 4}, {14, 1}};
        int[][] keyIndexMap1 = {{2, 6}, {8, 9}, {12, 2}, {6, 6}};
        int[][] magicHouseIndexMap1 = {{1, 8}, {7, 7}, {10, 13}, {11, 1}, {14, 7}};
        int[][] wolfIndexMap1 = {{4, 0}, {4, 12}, {11, 4}, {7, 6}};
        int[][] riverIndexMap1 = {{0, 0}, {1, 0}, {1, 1}, {2, 1}, {3, 1}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {6, 3}, {7, 3}, {8, 3}, {9, 3},
                {9, 4}, {9, 5}, {9, 6}, {9, 7}, {10, 7}, {10, 8}, {10, 9}, {10, 10}, {11, 10}, {12, 10}, {13, 10}, {13, 11}, {13, 12}, {14, 12}};

        for (int[] ints : monsterIndexMap1) {
            map1[ints[0]][ints[1]] = "Monster. It is maybe a strong Enemy.";
        }
        for (int[] ints : keyIndexMap1) {
            map1[ints[0]][ints[1]] = "Treasure. You can open and choose new Items. ";
        }
        for (int[] ints : magicHouseIndexMap1) {
            map1[ints[0]][ints[1]] = "Magic House. You can find some food and drink to raise your Health Points and Stamina Points";
        }
        for (int[] ints : wolfIndexMap1) {
            map1[ints[0]][ints[1]] = "Wild Animals. Big Challenge.";
        }
        for (int[] ints : riverIndexMap1) {
            map1[ints[0]][ints[1]] = "River, it is impossible to go. ";
        }
        return map1;
    }


    @Override
    public String[] getMapsNames() {
        return new String[]{map1Name,map2Name};
    }
}
