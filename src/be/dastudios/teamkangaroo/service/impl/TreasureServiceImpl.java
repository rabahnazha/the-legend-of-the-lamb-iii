package be.dastudios.teamkangaroo.service.impl;


import be.dastudios.teamkangaroo.entity.*;

import be.dastudios.teamkangaroo.service.TreasureService;

import java.util.Random;

public class TreasureServiceImpl implements TreasureService {

    private  Random RAND_NUM_GEN  =new Random();

    @Override
    public void openTreasure(Adventurer adventurer) {
        Inventory inventory = adventurer.getInventory();
        Item item = generateRandomItem();
        String itemInfo = "There is a "+ item.name() +" with "+item.getWeight()+" weight...";
        System.out.println(itemInfo);
        System.out.println("Do you wanna get  this item ");
        inventory.addItem(item);

    }

    private Item generateRandomItem(){
         Item[] items = Item.values();
         Item[] itemsFortreasure = new Item[7];
        for (int i = 12; i < 19; i++) {
            itemsFortreasure[i-12] = items[i];
        }
        Item randomItem = itemsFortreasure[RAND_NUM_GEN.nextInt(7)];
        return randomItem;
    }
}
