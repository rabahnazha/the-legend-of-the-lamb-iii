package be.dastudios.teamkangaroo.service.impl;

import be.dastudios.teamkangaroo.entity.Adventurer;
import be.dastudios.teamkangaroo.repository.GameRepo;
import be.dastudios.teamkangaroo.repository.GameRepoImpl;
import be.dastudios.teamkangaroo.service.FileService;
import be.dastudios.teamkangaroo.utility.KeyboardUtility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By Moon
 * 2/26/2021, Fri
 **/
public class FileServiceImpl implements FileService {
    private GameRepo gameRepo = new GameRepoImpl();

    @Override
    public String[] getAllSavedGamesNames() {
        String[] allNamesOfSavedGames =gameRepo.findAll();
        return allNamesOfSavedGames;
    }

    @Override
    public void createGameFolder(Adventurer adventurer) {
        String nameOfSavedGame= KeyboardUtility.ask("Give a name for your game");
        gameRepo.save(adventurer,nameOfSavedGame);
    }

    @Override
    public Adventurer getByName(String chosenFileName) {
        Adventurer adventurerInfo = gameRepo.getByFileName(chosenFileName);
        return adventurerInfo;

    }

    @Override
    public void resetAllSavedGame() {

        gameRepo.deleteAll();
    }
}
