package be.dastudios.teamkangaroo.service.impl;

import be.dastudios.teamkangaroo.entity.Adventurer;
import be.dastudios.teamkangaroo.service.FileService;
import be.dastudios.teamkangaroo.service.GeneralAppService;

import java.awt.*;

import static be.dastudios.teamkangaroo.utility.MenuUtility.center;
import static be.dastudios.teamkangaroo.utility.MenuUtility.tildaLine;

/**
 * Created By Moon
 * 2/23/2021, Tue
 **/
public class GeneralAppServiceImpl implements GeneralAppService {
    private FileService fileService =new FileServiceImpl();

    @Override
    public void quit() {
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        System.out.println(center("*** GOODBYE :) ***"));
        System.out.println(center("*** SEE YOU LATER ***"));
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        System.exit(0);

    }
    public void waitForLoading(int n) {
        for (int i = 0; i < n ; i++) {
            System.out.print(".");

            try {
                Toolkit.getDefaultToolkit().beep();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();

    }

    public void waitForFightTurns() {
        for (int i = 0; i < 5 ; i++) {
            System.out.print("-> ");

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();

    }

    @Override
    public void waitForCombat() {
        for (int i = 0; i < 3 ; i++) {
            System.out.print("->@ ");

            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }

    @Override
    public void saveAndQuit(Adventurer adventurer) {

       fileService.createGameFolder(adventurer);

        quit();

    }
}
