package be.dastudios.teamkangaroo.service.impl.combatimplementation;

import be.dastudios.teamkangaroo.entity.Adventurer;
import be.dastudios.teamkangaroo.entity.Attributes;
import be.dastudios.teamkangaroo.entity.enemy.Enemy;
import be.dastudios.teamkangaroo.entity.enemy.TIER3;
import be.dastudios.teamkangaroo.generator.RandomAdventurerGenerator;
import be.dastudios.teamkangaroo.generator.RandomEnemyGenerator;
import be.dastudios.teamkangaroo.service.combatservices.CombatService1;
import be.dastudios.teamkangaroo.service.GeneralAppService;
import be.dastudios.teamkangaroo.service.combatservices.CombatService3;
import be.dastudios.teamkangaroo.service.impl.GeneralAppServiceImpl;
import be.dastudios.teamkangaroo.utility.Dice;
import be.dastudios.teamkangaroo.utility.MenuUtility;

import java.util.Comparator;
import java.util.List;

import static be.dastudios.teamkangaroo.utility.KeyboardUtility.askForInt;
import static be.dastudios.teamkangaroo.utility.KeyboardUtility.askYOrN;

public class CombatServiceImplTier3 implements CombatService3 {
    final int adventurerBasicAttackDamagePoints = 10;
    private GeneralAppService generalAppService = new GeneralAppServiceImpl();


    @Override
    public void startCombat(Adventurer adventurer) {
        String askForTeamCreate = "Do you want to create your team for combat?";

        if (askYOrN(askForTeamCreate)) {
            String askForTeamCount = "How many team members do you want to have?";
            int countOfTeamMembers = askForInt(askForTeamCount);

            List<Adventurer> adventurerTeam = createAdventurersTeam(adventurer, countOfTeamMembers);
            System.out.println("Your team is being created!...");
            generalAppService.waitForLoading(3);
            showAdventurersTeam(adventurerTeam);

            List<TIER3> enemyTeam = createEnemiesTeam(countOfTeamMembers);
            System.out.println("Enemy team is being created!...");
            generalAppService.waitForLoading(3);
            showEnemiesTeam(enemyTeam);

            boolean turnOfAdv = isFirstTurnIsAdventurers(adventurerTeam.get(0), enemyTeam.get(0));

            if (turnOfAdv) {
                System.out.println("Fight starts with Adventurers Basic Attack!...");
                fightByAdventurer(adventurerTeam, enemyTeam);
            }else{
                System.out.println("Fight starts with Enemy's Basic Attack!...");
                fightByEnemy(adventurerTeam, enemyTeam);
            }
            if (enemyTeam.size()>0){
                System.out.println("GAME OVER!!!!");
                generalAppService.quit();
            }else{
                System.out.printf("Good job %s .Your TEAM is the ***WINNER***",adventurer.getName());
                adventurer.setExperiencePoint(adventurerTeam.size()*13);  //we can write another calculation too.

                System.out.println();
                System.out.println(MenuUtility.thickLine());
                generalAppService.waitForLoading(3);
                System.out.println(adventurer);
            }

        }else{
            List<Adventurer> adventurerTeam = createAdventurersTeam(adventurer, 0);
            generalAppService.waitForLoading(3);
            showAdventurersTeam(adventurerTeam);

            List<TIER3> enemyTeam = createEnemiesTeam(0);
            System.out.println("Enemy  is being created!...");
            generalAppService.waitForLoading(3);
            showEnemiesTeam(enemyTeam);

            boolean turnOfAdv = isFirstTurnIsAdventurers(adventurerTeam.get(0), enemyTeam.get(0));

            if (turnOfAdv) {
                System.out.println("Fight starts with Adventurers Basic Attack!...");
                fightByAdventurer(adventurerTeam, enemyTeam);
            }else{
                System.out.println("Fight starts with Enemy's Basic Attack!...");
                fightByEnemy(adventurerTeam, enemyTeam);
            }
            if (enemyTeam.size()>0){
                System.out.println("GAME OVER!!!!");
                generalAppService.quit();
            }else{
                System.out.printf("Good job %s .You are the ***WINNER***",adventurer.getName());
                adventurer.setExperiencePoint(adventurerTeam.size()*13);  //we can write another calculation too.

                System.out.println();
                System.out.println(MenuUtility.thickLine());
                generalAppService.waitForLoading(3);
                System.out.println(adventurer);
            }

        }


    }

    private void fightByEnemy(List<Adventurer> adventurerTeam, List<TIER3> enemyTeam) {
        int adventurerIndex = 0;
        int enemyIndex = 0;
        while (adventurerTeam.size() != 0 && enemyTeam.size() != 0) {
            Adventurer adventurer = adventurerTeam.get(adventurerIndex % adventurerTeam.size());
            Enemy enemy = enemyTeam.get(enemyIndex % enemyTeam.size());

            System.out.printf("%s  attacks  %s!.. ", enemy.getName(), adventurer.getName());
            System.out.printf("");

            basicAttackFromEnemyToAdventurer(adventurer,enemy);
            basicAttackFromAdventurerToEnemy(adventurer, enemy);

            if(enemy.getHealthPoint()<= 0){
                enemyTeam.remove(enemy);
            }
            if(adventurer.getHealthPoints()<=0){
                adventurerTeam.remove(adventurer);
            }
            adventurerIndex++;
            enemyIndex++;

            System.out.println(MenuUtility.thickLine());
            generalAppService.waitForFightTurns();
        }
    }

    private void showEnemiesTeam(List<TIER3> enemyTeam) {
        for (TIER3 tier3 : enemyTeam) {
            System.out.println(tier3);
        }
        System.out.println(MenuUtility.tildaLine());
        System.out.println(MenuUtility.tildaLine());
    }

    private void showAdventurersTeam(List<Adventurer> adventurerTeam) {
        for (Adventurer adventurer : adventurerTeam) {
            System.out.println(adventurer);
        }
        System.out.println(MenuUtility.tildaLine());
        System.out.println(MenuUtility.tildaLine());
    }

    private void fightByAdventurer(List<Adventurer> adventurerTeam, List<TIER3> enemyTeam) {
        int adventurerIndex = 0;
        int enemyIndex = 0;
        while (adventurerTeam.size() != 0 && enemyTeam.size() != 0) {
            Adventurer adventurer = adventurerTeam.get(adventurerIndex % adventurerTeam.size());
            Enemy enemy = enemyTeam.get(enemyIndex % enemyTeam.size());

            System.out.printf("%s  attacks  %s!.. ", adventurer.getName(), enemy.getName());
            System.out.printf("");

            basicAttackFromAdventurerToEnemy(adventurer, enemy);
            basicAttackFromEnemyToAdventurer(adventurer, enemy);

            if(enemy.getHealthPoint()<= 0){
                enemyTeam.remove(enemy);
            }
            if(adventurer.getHealthPoints()<=0){
                adventurerTeam.remove(adventurer);
            }
            adventurerIndex++;
            enemyIndex++;

            System.out.println(MenuUtility.thickLine());
            generalAppService.waitForFightTurns();
        }

    }

    private void basicAttackFromAdventurerToEnemy(Adventurer adventurer, Enemy enemy) {
        if(decideAttackSuccessForAdventurer(adventurer)){
            int updatedHP = enemy.getHealthPoint() - adventurerBasicAttackDamagePoints;

                enemy.setHealthPoint(enemy.getHealthPoint() - adventurerBasicAttackDamagePoints);
                System.out.printf("%s attacks %s with basic attack and deals %d damage. ", adventurer.getName(), enemy.getName(), adventurerBasicAttackDamagePoints);
                System.out.println("");
                System.out.printf("%s has %d HP left", enemy.getName(), enemy.getHealthPoint());
                System.out.println("");

            if(updatedHP <=0 ) {
                System.out.printf("%s is dead  with %d HP!..", enemy.getName(), enemy.getHealthPoint());
                System.out.println("");
            }

        }else{
            System.out.println("You have missed your target!!!");
        }


    }
    private void basicAttackFromEnemyToAdventurer(Adventurer adventurer, Enemy enemy) {
        if(decideAttackSuccessForEnemy(adventurer)){
            int updatedHP = adventurer.getHealthPoints() - enemy.getAttackDamage();

            adventurer.setHealthPoints( adventurer.getHealthPoints() - enemy.getAttackDamage());
            System.out.printf("%s attacks %s with basic attack and deals %d damage",enemy.getName(),adventurer.getName(),enemy.getAttackDamage());
            System.out.println("");
            System.out.printf("%s has %d HP left", adventurer.getName(), adventurer.getHealthPoints());
            System.out.println("");

            if(updatedHP <= 0) {
                System.out.printf("%s is dead  with %d HP!..", adventurer.getName(), adventurer.getHealthPoints());
                System.out.println("");
            }

        }else{
            System.out.println("Enemy has missed the target!!!");
        }


    }

    private boolean decideAttackSuccessForAdventurer(Adventurer adventurer) {
        int diceSide = adventurer.getAttributesList().get(Attributes.DEXTERITY);
        int adventurerDicePoints = Dice.roll(diceSide);
        int enemyDicePoints = Dice.roll(diceSide);

        return adventurerDicePoints >= enemyDicePoints;
    }
    private boolean decideAttackSuccessForEnemy(Adventurer adventurer) {
        int diceSide = adventurer.getAttributesList().get(Attributes.DEXTERITY);
        int adventurerDicePoints = Dice.roll(diceSide);
        int enemyDicePoints = Dice.roll(diceSide);

        return enemyDicePoints >= adventurerDicePoints;
    }


    private List<TIER3> createEnemiesTeam(int countOfTeamMembers) {
        RandomEnemyGenerator randomEnemyGenerator = new RandomEnemyGenerator();

        randomEnemyGenerator.createTeam(countOfTeamMembers + 1);
        List<TIER3> enemyTeam = randomEnemyGenerator.getListTier3();

        enemyTeam.sort(new Comparator<Enemy>() {
            @Override
            public int compare(Enemy o1, Enemy o2) {
                return o1.getAttributePoints() - o2.getAttributePoints();
            }
        });
        return enemyTeam;
    }

    private List<Adventurer> createAdventurersTeam(Adventurer adventurer, int countOfTeamMembers) {
        RandomAdventurerGenerator randomAdventurerGenerator = new RandomAdventurerGenerator();

        List<Adventurer> adventurerTeam = randomAdventurerGenerator.createTeam(countOfTeamMembers);
        adventurerTeam.add(0, adventurer);

        adventurerTeam.sort(new Comparator<Adventurer>() {
            @Override
            public int compare(Adventurer o1, Adventurer o2) {
                return o1.getAttributesList().get(Attributes.DEXTERITY) - o2.getAttributesList().get(Attributes.DEXTERITY);
            }
        });
        return adventurerTeam;
    }

    private boolean isFirstTurnIsAdventurers(Adventurer adventurer, Enemy tier1) {
        if (calculateAdventurerTurnPoints(adventurer) >= calculateEnemyTurnPoints(tier1)) {
            return true;
        } else {
            return false;
        }
    }

    private int calculateAdventurerTurnPoints(Adventurer adventurer) {
        int adventurerTotalTurnPoints = 0;
        int dicePoint = Dice.roll(20);
        int adventurerInitiativePoints = adventurer.getInitiative();
        int adventurerDextirityPoints = adventurer.getAttributesList().get(Attributes.DEXTERITY);

        adventurerTotalTurnPoints = dicePoint + adventurerInitiativePoints + (adventurerDextirityPoints / 2);
        return adventurerTotalTurnPoints;

    }

    private int calculateEnemyTurnPoints(Enemy enemy) {
        int enemyTotalTurnPoints = 0;
        int dicePoint = Dice.roll(20);
        int enemyInitiativePoints = RandomEnemyGenerator.generateRandomInitiativePoints();
        int enemyAttributesPoints = enemy.getAttributePoints();


        enemyTotalTurnPoints = dicePoint + enemyInitiativePoints + (enemyAttributesPoints / 2);
        return enemyTotalTurnPoints;


    }
}
