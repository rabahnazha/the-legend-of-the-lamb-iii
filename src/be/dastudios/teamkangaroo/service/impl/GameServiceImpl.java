package be.dastudios.teamkangaroo.service.impl;

import be.dastudios.teamkangaroo.entity.Adventurer;
import be.dastudios.teamkangaroo.service.combatservices.CombatService1;
import be.dastudios.teamkangaroo.service.GameService;
import be.dastudios.teamkangaroo.service.GeneralAppService;
import be.dastudios.teamkangaroo.service.TreasureService;
import be.dastudios.teamkangaroo.service.combatservices.CombatService2;
import be.dastudios.teamkangaroo.service.combatservices.CombatService3;
import be.dastudios.teamkangaroo.service.combatservices.CombatService4;
import be.dastudios.teamkangaroo.service.impl.combatimplementation.CombatServiceImplTier1;
import be.dastudios.teamkangaroo.service.impl.combatimplementation.CombatServiceImplTier2;
import be.dastudios.teamkangaroo.service.impl.combatimplementation.CombatServiceImplTier3;
import be.dastudios.teamkangaroo.service.impl.combatimplementation.CombatServiceImplTier4;

import static be.dastudios.teamkangaroo.utility.KeyboardUtility.*;
import static be.dastudios.teamkangaroo.utility.MenuUtility.tildaLine;

public class GameServiceImpl implements GameService {
   private CombatService1 combatService1 = new CombatServiceImplTier1();
   private CombatService2 combatService2 = new CombatServiceImplTier2();
   private CombatService3 combatService3 = new CombatServiceImplTier3();
   private CombatService4 combatService4 = new CombatServiceImplTier4();
   private TreasureService treasureService = new TreasureServiceImpl();
   private Adventurer adventurer=new Adventurer();
   private GeneralAppService generalAppService = new GeneralAppServiceImpl();

   private  int xAdv = 8;
    private  int yAdv = 7;
    private int chosenMapIndex;


    @Override
    public void startGame(Adventurer adventurer, String[][] map, int chosenMapIndex,int xAdv,int yAdv) {
        this.adventurer = adventurer;
        this.chosenMapIndex=chosenMapIndex;
        if(xAdv>=0){
            this.xAdv=xAdv;
            this.yAdv=yAdv;
        }
        lookDirection(map);
        chooseMoveDirection(map);
        continueGame(map);

    }
    private void getPositionOfAdventurer(){
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        String positionOfAdventurer = "The position Of The Adventurer is x = %d , y = %d \n";
        System.out.printf(positionOfAdventurer,xAdv,yAdv);
        System.out.println(tildaLine());
        System.out.println(tildaLine());
    }

    private  void continueGame(String[][] map) {
        String adventurerLocation = map[xAdv][yAdv];
        if(adventurerLocation == null) {
            lookDirection(map);
            chooseMoveDirection(map);
        }else if(adventurerLocation.contains("House")){
            System.out.println("Great. You can rest , eat  and drink . You will get extra 5 points for your Health point");
            generalAppService.waitForFightTurns();
            int oldHP = adventurer.getHealthPoints();
            adventurer.setHealthPoints(oldHP + 5);
            System.out.printf("Your old Health Point was  %d and now %d. Move time :)",oldHP,adventurer.getHealthPoints());
            System.out.println("");
            lookDirection(map);
            chooseMoveDirection(map);
        }else if(adventurerLocation.contains("Monster")){
            System.out.println("Are you ready for combat. ");
            combatService1.startCombat(adventurer);
            lookDirection(map);
            chooseMoveDirection(map);
        }else if(adventurerLocation.contains("Wild")){
            System.out.println("Are you ready to fight with Wild Animals. Is not offered if you are in Level1. But may be funny:)");
            combatService4.startCombat(adventurer);
            lookDirection(map);
            chooseMoveDirection(map);
        }else if(adventurerLocation.contains("Treasure")){
            treasureService.openTreasure(adventurer);
            lookDirection(map);
            chooseMoveDirection(map);
        }
    }

    private  void chooseMoveDirection(String[][] map) {
        String askToMove = "Which direction do you wanna go?";
        String chosenDirection = ask(askToMove);
        String keyNorth= "north";
        String keySouth= "south";
        String keyEast= "east";
        String keyWest= "west";
        if(chosenDirection.contains(keyNorth)){
            try {
                if( map[xAdv-1][yAdv] !=null && map[xAdv-1][yAdv].contains("River")){
                    System.out.println("There is no path. Choose another Path pls.");
                    chooseMoveDirection(map);
                }else {
                    xAdv--;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("You're trying to get out of the bounds. Please choose another direction ");
                getPositionOfAdventurer();
                chooseMoveDirection(map);
            }
        }else if(chosenDirection.contains(keySouth)) {
            try {
                if(map[xAdv+1][yAdv] !=null && map[xAdv+1][yAdv].contains("River")){
                    System.out.println("There is no path. Choose another Path pls.");
                    chooseMoveDirection(map);
                }else {
                    xAdv++;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("You're trying to get out of the bounds. Please choose another direction ");
                getPositionOfAdventurer();
                chooseMoveDirection(map);
            }
        }else if(chosenDirection.contains(keyEast)) {
            try {
                if(map[xAdv][yAdv+1] !=null && map[xAdv][yAdv+1].contains("River")){
                    System.out.println("There is no path. Choose another Path pls.");
                    chooseMoveDirection(map);
                }else{
                    yAdv++;}
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("You're trying to get out of the bounds. Please choose another direction ");
                getPositionOfAdventurer();
                chooseMoveDirection(map);
            }
        }else if(chosenDirection.contains(keyWest)) {
            try {
                if(map[xAdv][yAdv-1] != null && map[xAdv][yAdv-1].contains("River")){
                    System.out.println("There is no path. Choose another Path pls.");
                    chooseMoveDirection(map);
                }else{
                    yAdv--;}
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("You're trying to get out of the bounds. Please choose another direction ");
                getPositionOfAdventurer();
                chooseMoveDirection(map);
            }
        }else{
            System.out.println("There is no Direction as you write. Write a valid  Direction  please.");
            chooseMoveDirection(map);
        }
        continueGame( map);
    }

    private  void lookDirection( String[][] map) {
        String North;
        String South;
        String East;
        String West;
        if(xAdv-1 >= 0) {
            North = map[xAdv - 1][yAdv];
        }else{
            North = " very High Wall.Out Of Bounds for your Map.";
        } if(xAdv+1<15) {
            South = map[xAdv + 1][yAdv];
        }else{
            South = " very High Wall.Out Of Bounds for your Map.";
        } if(yAdv+1<15) {
            East = map[xAdv][yAdv + 1];
        }else{
            East = " very High Wall.Out Of Bounds for your Map.";
        } if(yAdv-1>=0) {
            West = map[xAdv][yAdv-1];
        }else{
            West = " very High Wall.Out Of Bounds for your Map.";
        }


        String northInfo = "To the north of you there is a %s ";
        String southInfo = "To the south of you there is a %s ";
        String eastInfo = "To the east of you there is a %s ";
        String westInfo = "To the west of you there is a %s ";
        String nullInfo = " free Path. You can go from there.";


        getPositionOfAdventurer();
        String askToDo = "What do you want to do?";
        String[] lookMenu = {"Look West", "Look East", "Look North", "Look South", "Look Around", "Save the  Game and Quit"};
        int chosenLookDirection = askForChoice(askToDo, lookMenu);
        switch (chosenLookDirection) {
            case 0:
                if (West != null) {
                    System.out.printf(westInfo, West);
                    System.out.println();
                } else {
                    System.out.printf(westInfo, nullInfo);
                }
                break;
            case 1:
                if (East != null) {
                    System.out.printf(eastInfo, East);
                    System.out.println();
                } else {
                    System.out.printf(eastInfo, nullInfo);
                }
                break;
            case 2:
                if (North != null) {
                    System.out.printf(northInfo, North);
                    System.out.println();
                } else {
                    System.out.printf(northInfo, nullInfo);
                }
                break;
            case 3:
                if (South != null) {
                    System.out.printf(southInfo, South);
                    System.out.println();
                } else {
                    System.out.printf(southInfo, nullInfo);
                }
                break;
            case 4:
                if (West != null) {
                    System.out.printf(westInfo, West);

                } else {
                    System.out.printf(westInfo, nullInfo);
                }
                System.out.println();
                if (East != null) {
                    System.out.printf(eastInfo, East);

                } else {
                    System.out.printf(eastInfo, nullInfo);
                }
                System.out.println();
                if (North != null) {
                    System.out.printf(northInfo, North);

                } else {
                    System.out.printf(northInfo, nullInfo);
                }
                System.out.println();
                if (South != null) {
                    System.out.printf(southInfo, South);

                } else {
                    System.out.printf(southInfo, nullInfo);
                }
                System.out.println();
                break;

            case 5:
                //int[] locationAndMapInfo = {xAdv,yAdv,chosenMapIndex};
                adventurer.setLocationInfo(xAdv,yAdv,chosenMapIndex);

                generalAppService.saveAndQuit(adventurer);
                break;

        }

    }
}
