package be.dastudios.teamkangaroo.service.impl;


import be.dastudios.teamkangaroo.entity.Adventurer;
import be.dastudios.teamkangaroo.generator.AdventurerGenerator;
import be.dastudios.teamkangaroo.service.*;

import java.util.HashMap;
import java.util.Map;

import static be.dastudios.teamkangaroo.constant.AppConstants.*;
import static be.dastudios.teamkangaroo.utility.KeyboardUtility.*;
import static be.dastudios.teamkangaroo.utility.TextUtil.*;

public class ManagementServiceImpl implements ManagementService {
    private GeneralAppService generalAppService = new GeneralAppServiceImpl();
    private MapService mapService = new MapServiceImpl();
    private GameService gameService = new GameServiceImpl();
    private FileService fileService = new FileServiceImpl();

    @Override
    public void start() {
        printTitle(askForGameTitle);
        printSubheading(askForCompany);
        System.out.println(askForLoadingMaps);
        generalAppService.waitForLoading(3);
        printTitle(askForReadyToStart);
        showMenuList();

    }

    private void showMenuList() {
        int chosenMenuOption = askForChoice(askForMenu,menuOptions);
        switch(chosenMenuOption){
            case 0:
                startNewGame();
                break;
            case 1:
                loadASavedGame();
                break;
            case 2:
                resetAllSavedGame();
                break;
            case 3:
                gameControlMenu();
                break;
            case 4:
                gameSettingsMenu();
                break;
            case 5:
                generalAppService.quit();
                break;
        }
    }

    private void gameSettingsMenu() {
        System.out.println("Our Settings are in progress... Try Later please");
        showMenuList();
    }

    private void gameControlMenu() {
        System.out.println("> Controls: ");
        System.out.println("Go <direction>: go in direction (north, south, west, east)");
        System.out.println("Attack <Target>: attack the specified target with main weapon");
        System.out.println("Attack <Target>: attack the specified target with main weapon");
        System.out.println("Look <direction>: get a description of the next tile in the " +
                "given direction (omitting he direction means you get a description of all tiles around you)\n");
        System.out.println("save <savedNames>: saves the game");
        System.out.println("Quit: goes back to main menu");
        generalAppService.waitForLoading(2);
        showMenuList();

    }

    private void resetAllSavedGame() {
        System.out.println("Deleting All Saved Games...");
        generalAppService.waitForFightTurns();
        fileService.resetAllSavedGame();
        showMenuList();
    }

    private void loadASavedGame() {
        if(fileService.getAllSavedGamesNames() != null){
        String[] fileNames = fileService.getAllSavedGamesNames();
        System.out.println("Loading all saved games");
        generalAppService.waitForLoading(3);
        String message = "Which game do you wanna load?";

            int chosenGameIndex = askForChoice(message, fileNames);
            String chosenFileName = fileNames[chosenGameIndex];
            Adventurer adventurerInfo = fileService.getByName(chosenFileName);
            startGameWithParseAdventurerInfo(adventurerInfo);
        }else{
            showMenuList();
        }
    }

    private void startGameWithParseAdventurerInfo(Adventurer adventurer) {

            System.out.println(adventurer);
            int[] adventurerPositionsAndMapInfo = adventurer.getLocationInfo();
            int xAdv = adventurerPositionsAndMapInfo[0];
            int yAdv = adventurerPositionsAndMapInfo[1];
            int chosenMapIndex = adventurerPositionsAndMapInfo[2];
            generalAppService.waitForLoading(3);
            String[][] map = createMap(chosenMapIndex);
            gameService.startGame(adventurer,map,chosenMapIndex,xAdv,yAdv);
        }



    public String askForChooseMap  = "What map you want to play?";
    private void startNewGame() {
        int chosenMapIndex = askForChoice(askForChooseMap,mapService.getMapsNames());
        String adventurersName = askForName();
        String genderName = askForGender(adventurersName);
        String raceName = chooseForRace();
        String professionName= chooseForProfession().toLowerCase();
        createAdventurer(adventurersName, genderName ,raceName,professionName,chosenMapIndex);


    }

    private  String[][] createMap(int chosenMapIndex) {
        if(chosenMapIndex==0) return mapService.createMap1();
        else return mapService.createMap2();
    }

    private void createAdventurer(String adventurersName, String genderName, String raceName, String professionName, int chosenMapIndex) {
        AdventurerGenerator adventurerGenerator = new AdventurerGenerator(adventurersName,genderName,raceName,professionName);
        Adventurer adventurer = adventurerGenerator.generateAdventurer();
        System.out.println(creatingCharacterInfo);
        generalAppService.waitForLoading(3);
        System.out.println(adventurer);
        String[][] map = createMap(chosenMapIndex);
        gameService.startGame(adventurer,map, chosenMapIndex,-1,-1);
    }



}
