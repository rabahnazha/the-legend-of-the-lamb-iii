package be.dastudios.teamkangaroo.service;

import be.dastudios.teamkangaroo.entity.Adventurer;

/**
 * Created By Moon
 * 2/23/2021, Tue
 **/
public interface GeneralAppService {
    void quit();
    void waitForLoading(int n);
    void waitForFightTurns();
    void waitForCombat();

    void saveAndQuit(Adventurer adventurer);
}
