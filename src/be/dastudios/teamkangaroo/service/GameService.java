package be.dastudios.teamkangaroo.service;

import be.dastudios.teamkangaroo.entity.Adventurer;

public interface GameService {
    void startGame(Adventurer adventurer, String[][] map, int chosenMapIndex,int xAdv,int yAdv);
}
