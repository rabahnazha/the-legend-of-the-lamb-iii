package be.dastudios.teamkangaroo.service;


public interface MapService {
    String[][] createMap1();
    String[][] createMap2();
    String[] getMapsNames();

}
