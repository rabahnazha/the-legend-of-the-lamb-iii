package be.dastudios.teamkangaroo.constant;


public abstract class AppConstants {
    public static  String askForGameTitle = "++ The Legend of the Lamb III ++ ";
    public static  String askForCompany   = "--     D.A Studios Belgium    -- ";
    public static  String askForLoadingMaps = "Loading maps and Save Games";
    public static  String askForReadyToStart = "++   DONE   ++";
    public static  String askForMenu = "What do you want to do?";

    public static  String askForNameOfAdventurer = "What is your Adventurer's name?";
    public static String afterNameText = "...Yes...that's a good name. A strong name. One that will be remembered " +
            "for generations.";
    public static  String askForChooseGender = "Tell me %s a male or a female \n";
    public static  String askForRace = "Can you tell me more about him/her, such as what race he/she is? \n"
            + "Human\n"+ "Elf\n"+ "Dwarf";
    public static  String askForProfession = "What is his/her class(profession)?\n"
            + "Warrior\n"+ "Mage\n"+ "Ranger\n"+ "Rogue";
    public static  String creatingCharacterInfo = "Your character is being created";
    public static String[] menuOptions ={"New - Start a New Game", "Load - Load a Saved Game",
            "Reset - Reset All Saved Files","Controls - Game Controls", "Settings - Game Settings" ,"Quit - Quit Game"};

}
