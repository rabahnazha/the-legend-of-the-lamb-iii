package be.dastudios.teamkangaroo.entity;

import be.dastudios.teamkangaroo.entity.enemy.TIER1;
import be.dastudios.teamkangaroo.entity.profession.Profession;
import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactory;

import java.io.Serializable;
import java.util.*;

import static be.dastudios.teamkangaroo.entity.Attributes.*;
import static be.dastudios.teamkangaroo.utility.MenuUtility.center;
import static be.dastudios.teamkangaroo.utility.MenuUtility.tildaLine;

public class Adventurer implements Serializable {

    private final static int healthAndStaminaPower = 5;

    private String name;
    private Gender gender;
    private Race race;
    private Inventory inventory;
    private  int initialHealthPoints ;
    private int healthPoints;
    private  int initialStaminaPoints;
    private int staminaPoints;
    private ProfessionFactory professionFactory;
    private Map<Attributes, Integer> attributesList = new HashMap<>();
    private int initiative;
    private int level = 1;
    private int experiencePoint = 0;
    private int[] locationInfo=new int[3];


    {
        attributesList.put(STRENGTH, 10);
        attributesList.put(INTELLIGENCE, 10);
        attributesList.put(WISDOM, 10);
        attributesList.put(DEXTERITY, 10);
        attributesList.put(CONSTITUTION, 10);
        attributesList.put(CHARISMA, 10);
    }

    public Adventurer(String name, Gender gender) {
        this.name = name;
        this.gender = gender;
        this.initiative=5;

    }

    public Adventurer() {
    }

    public int[] getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(int xAdv, int yAdv, int chosenMapIndex) {
        locationInfo[0]=xAdv;
        locationInfo[1]=yAdv;
        locationInfo[2]=chosenMapIndex;
    }

    public void calculateInitialStaminaPoints() {
        initialStaminaPoints = attributesList.get(DEXTERITY) * healthAndStaminaPower;
    }
    public void calculateStaminaPoints() {
        staminaPoints = attributesList.get(DEXTERITY) * healthAndStaminaPower;
    }
    public void calculateInitialHealthPoints() {
        initialHealthPoints = attributesList.get(CONSTITUTION) * healthAndStaminaPower;
    }
    public void calculateHealthPoints() {
        healthPoints = attributesList.get(CONSTITUTION) * healthAndStaminaPower;
    }

    public int getInitialStaminaPoints() {
        return initialStaminaPoints;
    }

    public int getStaminaPoints() {
        return staminaPoints;
    }

    public void setStaminaPoints(int staminaPoints) {
        this.staminaPoints = staminaPoints;
    }

    public int getInitialHealthPoints() {
        return initialHealthPoints;
    }


    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    public String getName() { // static added
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
        updateAttributesValueByRaceBonusPoints();

    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public ProfessionFactory getProfessionFactory() {
        return professionFactory;
    }

    public void setProfessionFactory(ProfessionFactory professionFactory) {
        this.professionFactory = professionFactory;
        updateAttributesValueByProfessionBonusPoints();
        updateInitiativeByProfessionInitiativeBonusPoints();
        calculateHealthPoints();
        calculateStaminaPoints();
        calculateInitialHealthPoints();
        calculateInitialStaminaPoints();
    }

    public Map<Attributes, Integer> getAttributesList() {
        return attributesList;
    }

    public void setAttributesList(Map<Attributes, Integer> attributesList) {
        this.attributesList = attributesList;
    }

    public int getInitiative() {
        return initiative;
    }

    public void setInitiative(int initiative) {
        this.initiative = initiative;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExperiencePoint() {
        return experiencePoint;
    }

    public void setExperiencePoint(int experiencePoint) {
        this.experiencePoint = experiencePoint;
    }

    private void updateAttributesValueByRaceBonusPoints() {
        int bonusRacePoints = getRace().getBonusPoints();
        List<Attributes> attributesOfRaceList = getRace().getAttributesList();


        for (Attributes attributeAdventurer : attributesList.keySet()) {
            int attributesValue = attributesList.get(attributeAdventurer);
            for (Attributes attributes : attributesOfRaceList) {
                if (attributeAdventurer.equals(attributes)) {
                    attributesValue += bonusRacePoints;

                }
            }
            attributesList.put(attributeAdventurer, attributesValue);
        }

    }

    private void updateAttributesValueByProfessionBonusPoints() {
        for (Attributes attributeAdventurer : attributesList.keySet()) {
            int attributesValue = attributesList.get(attributeAdventurer);
             for (Attributes attributes : getProfessionFactory().getClassBonus().keySet()) {
                if (attributeAdventurer.equals(attributes)) {
                    attributesValue += getProfessionFactory().getClassBonus().get(attributes);

                }
            }
            attributesList.put(attributeAdventurer, attributesValue);

        }

    }

    private void updateInitiativeByProfessionInitiativeBonusPoints() {
        int initiativePoint = getInitiative();
        initiativePoint += getProfessionFactory().getInitiativeBonus();
        setInitiative(initiativePoint);
    }

    private StringBuilder getRaceBonusPointsForAttributes() {
        StringBuilder allBonusInfos = new StringBuilder();
        for (Attributes attributes : getRace().getAttributesList()) {
            String bonusinfo = attributes.name() + " = +" + getRace().getBonusPoints() + "   ";
            allBonusInfos.append(bonusinfo);
        }
        return allBonusInfos;

    }

    @Override
    public String toString() {

        String nameInfo = getName().toUpperCase();
        String genderInfo = "Gender: " + getGender().toString();
        String raceInfo = "Race: " + getRace().toString() + "   ***  Bonus Points of Race : ";

        String levelInfo = "Level: " + level;
        String professionInfo = "Profession: " + getProfessionFactory().toString().toUpperCase();
        String xp = "Experience Points(XP) : " + experiencePoint + "/99";
        String initiativeInfo = "Initiative: " + getInitiative();
        String hp = "Health Points(): " + healthPoints + "/"+ initialHealthPoints;
        String sp = "Stamina Points(): " + staminaPoints + "/"+ initialStaminaPoints;
        String attributeNames = "";
        String attributePoints = "";
        for (Attributes attr : getAttributesList().keySet()) {
            attributeNames += attr.toString() + "   ";
            attributePoints += getAttributesList().get(attr) + "           ";
        }

        return  tildaLine()
                + tildaLine()
                + "\n"
                + center(nameInfo)
                + "\n"
                + genderInfo
                + "\n"
                + raceInfo
                + getRaceBonusPointsForAttributes()
                + "\n"
                + levelInfo
                + "\n"
                + xp
                + "\n"
                + hp
                + "\n"
                + sp
                + "\n"
                + initiativeInfo
                + "\n"
                + professionInfo
                + "\n"
                + attributeNames
                + "\n"
                + attributePoints
                + "\n"
                + tildaLine()
                + tildaLine();

    }



}
