package be.dastudios.teamkangaroo.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public enum Attributes implements Serializable {
    STRENGTH(10),
    INTELLIGENCE(10),
    WISDOM(10),
    DEXTERITY(10),
    CONSTITUTION(10),
    CHARISMA(10);

    private static final Map<Attributes, Integer> BY_VALUE = new HashMap<>();

    static {
        for (Attributes attr : values()) {
            BY_VALUE.put(attr, 10);
        }
    }

    private int value;

    Attributes(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
