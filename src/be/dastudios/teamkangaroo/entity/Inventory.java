package be.dastudios.teamkangaroo.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static be.dastudios.teamkangaroo.utility.KeyboardUtility.*;

public class Inventory implements Serializable {
    private List<Item> itemList;
    private final double MAX_INVENTORY_WEIGHT = 13;
    private double inventoryWeight;


    public Inventory(List<Item> itemList) {
        this.itemList = itemList;
        for (Item item : itemList) {
            inventoryWeight += item.getWeight();
        }
    }

    public void addItem(Item newItem) {
        if (inventoryWeight + newItem.getWeight() <= MAX_INVENTORY_WEIGHT) {
            itemList.add(newItem);
            inventoryWeight += newItem.getWeight();
            System.out.println("You have added item from treasure successful. Your new Inventory Weight is :  " + inventoryWeight);
        } else {
            if (askYOrN("Do you want to take this item by removing another item?")) {
                int result = showItemList();
                if (result == 1) {
                    addItem(newItem);
                }
            }
        }
    }

    public int showItemList() {
        String[] itemArray = new String[itemList.size()];
        for (int i = 0; i < itemArray.length; i++) {
            itemArray[i] = itemList.get(i).name() + " : " + itemList.get(i).getWeight();
        }
        if (itemList.size() > 0) {
            String message = "Look at the items list in your inventory and choose one to remove";
            int removedIndex = askForChoice(message, itemArray);
            return removeItemFromInventory(removedIndex);
        }
        return 0;
    }


    public int removeItemFromInventory(int removedIndex) {
        Item removedItem = itemList.remove(removedIndex);
        inventoryWeight -= removedItem.getWeight();
        return 1;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public double getMAX_INVENTORY_WEIGHT() {
        return MAX_INVENTORY_WEIGHT;
    }

    public double getInventoryWeight() {
        return inventoryWeight;
    }

    public void setInventoryWeight(double inventoryWeight) {
        this.inventoryWeight = inventoryWeight;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "itemList=" + itemList +
                ", MAX_INVENTORY_WEIGHT=" + MAX_INVENTORY_WEIGHT +
                ", inventoryWeight=" + inventoryWeight +
                '}';
    }
}


