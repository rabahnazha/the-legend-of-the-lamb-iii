package be.dastudios.teamkangaroo.entity;

import java.io.Serializable;

public enum Item implements Serializable {
    CASTLE_FORGED_ARMING_SWORD(4),
    KITE_SHIELD(5),
    ROYAL_BLUE_BRIGANDINE_GAMBESON(3),
    WONKY_LOOKING_WAND(1),
    BURGUNDY_ROBES(1),
    WEAK_MANA_POTION(1),
    YEW_WARBOW(2),
    X20_BARBED_HEAD_ARROWS(1),
    LEATHER_CHEST_ARMOUR(2),
    SHORT_STEEL_DAGGER(2),
    BLACK_HOODED_ROBES(1),
    X10_LOCKPICKS(1),
    GOLDEN_SWORD(5),
    HELMET(5),
    VEST(5),
    FLASH_LAMP(5),
    KNIFE(5),
    ENERGY_DRINK(4),
    SLEEPING_BAG(6);

    private int weight;

    Item(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}






