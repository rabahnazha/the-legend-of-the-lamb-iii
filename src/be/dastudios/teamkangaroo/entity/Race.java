package be.dastudios.teamkangaroo.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static be.dastudios.teamkangaroo.entity.Attributes.*;

public enum Race implements Serializable {
    HUMAN("Human",1,new ArrayList<>(Arrays.asList(STRENGTH, CONSTITUTION,CHARISMA,INTELLIGENCE,DEXTERITY,WISDOM))),
    DWARF("Dwarf",3,new ArrayList<>(Arrays.asList(STRENGTH, CONSTITUTION))),
    ELF("Elf",3,new ArrayList<>(Arrays.asList(DEXTERITY, INTELLIGENCE)));
    private String raceName;
    private int bonusPoints;
    private List<Attributes> attributesList;

    Race(String raceName, int bonusPoints, List<Attributes> attributesList) {
        this.raceName = raceName;
        this.bonusPoints = bonusPoints;
        this.attributesList = attributesList;
    }

    public String getRaceName() {
        return raceName;
    }

    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }

    public int getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(int bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public List<Attributes> getAttributesList() {
        return attributesList;
    }

    public void setAttributesList(List<Attributes> attributesList) {
        this.attributesList = attributesList;
    }
}

