package be.dastudios.teamkangaroo.entity;

import java.io.Serializable;

public enum Gender implements Serializable {
    MALE,
    FEMALE
}
