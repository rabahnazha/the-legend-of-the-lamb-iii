package be.dastudios.teamkangaroo.entity.profession;

import be.dastudios.teamkangaroo.entity.Attributes;
import be.dastudios.teamkangaroo.entity.Item;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Profession implements Serializable {
    public int healthPoints;
    public int staminaPoints;
    public List<Item> startingItems;
    public Map<Attributes, Integer> classBonus = new HashMap<>();
    public int initiativeBonus;

    public int getStaminaPoints() {
        return staminaPoints;
    }

    public void setStaminaPoints(int staminaPoints) {
        this.staminaPoints = staminaPoints;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    public List<Item> getStartingItems() {
        return startingItems;
    }

    public void setStartingItems(List<Item> startingItems) {
        this.startingItems = startingItems;
    }

    public Map<Attributes, Integer> getClassBonus() {
        return classBonus;
    }

    public void setClassBonus(Map<Attributes, Integer> classBonus) {
        this.classBonus = classBonus;
    }

    public int getInitiativeBonus() {
        return initiativeBonus;
    }

    public void setInitiativeBonus(int initiativeBonus) {
        this.initiativeBonus = initiativeBonus;
    }
}
