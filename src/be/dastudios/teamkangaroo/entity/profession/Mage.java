package be.dastudios.teamkangaroo.entity.profession;

import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static be.dastudios.teamkangaroo.entity.Attributes.*;
import static be.dastudios.teamkangaroo.entity.Item.*;
import static be.dastudios.teamkangaroo.entity.profession.Spells.*;

public class Mage extends Profession implements ProfessionFactory {
    private final static int classBonusOfWisdom = 3;
    private final static int classBonusOfConstitution = 0;
    private final static int classBonusOfIntelligence = 2;
    private final static int healthPower = 5;

    private List<Spells> startingSpells;
    {
        classBonus.put(WISDOM, classBonusOfWisdom);
        classBonus.put(INTELLIGENCE, classBonusOfIntelligence);

    }

    public Mage() {
        startingSpells = Stream.of(LIGHTING_BOLT).collect(Collectors.toList());
        startingItems = Stream.of(WONKY_LOOKING_WAND, BURGUNDY_ROBES, WEAK_MANA_POTION)
                .collect(Collectors.toList());

        healthPoints = (CONSTITUTION.getValue() + classBonusOfConstitution) * healthPower;
    }


    @Override
    public String getName() {
        return "mage";
    }

    public List<Spells> getStartingSpells() {
        return startingSpells;
    }

    @Override
    public String toString() {
        String name = getName();
        String classBonusInfo = "Class Bonus : +"+classBonusOfWisdom+" Wisdom, +"+classBonusOfIntelligence + " Intelligence";
        String startingSpells = "Starting Spells : " + getStartingSpells().toString();
        String startingItems = "Starting Items:  " + super.startingItems.toString();

        return name + "\n"
                + "------------------------------------------------------------\n"
                +classBonusInfo + "\n"
                + "------------------------------------------------------------\n"
                +startingSpells + "\n"
                + "------------------------------------------------------------\n"
                +startingItems+ "\n"
                + "------------------------------------------------------------\n";

    }

}
