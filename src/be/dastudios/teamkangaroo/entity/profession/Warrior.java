package be.dastudios.teamkangaroo.entity.profession;

import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static be.dastudios.teamkangaroo.entity.Attributes.*;
import static be.dastudios.teamkangaroo.entity.profession.Skills.*;
import static be.dastudios.teamkangaroo.entity.Item.*;


public class Warrior extends Profession implements ProfessionFactory {
    private final static int classBonusOfStrength = 3;
    private final static int classBonusOfConstitution = 2;
    private final static int healthPower = 5;

    private List<Skills> startingSkills;
    {
        classBonus.put(STRENGTH, classBonusOfStrength);
        classBonus.put(CONSTITUTION, classBonusOfConstitution);

    }

    public Warrior() {
        startingSkills = Stream.of(SHIELD_BASH).collect(Collectors.toList());
        startingItems = Stream.of(CASTLE_FORGED_ARMING_SWORD, KITE_SHIELD, ROYAL_BLUE_BRIGANDINE_GAMBESON)
                .collect(Collectors.toList());

        healthPoints = (CONSTITUTION.getValue() + classBonusOfConstitution) * healthPower;
    }

    public static int getHealthPower() {
        return healthPower;
    }

    public List<Skills> getStartingSkills() {
        return startingSkills;
    }

    public void setStartingSkills(List<Skills> startingSkills) {
        this.startingSkills = startingSkills;
    }

    @Override
    public String getName() {
        return "warrior";
    }
    @Override
    public String toString() {
        String name = getName();
        String classBonusInfo = "Class Bonus : +"+classBonusOfStrength+" Strength,+"+classBonusOfConstitution+" Constitution";
        String startingSkills = "Starting Skills : " + getStartingSkills().toString();
        String startingItems = "Starting Items:  " + super.startingItems.toString();

        return name + "\n"
                + "------------------------------------------------------------\n"
                +classBonusInfo + "\n"
                + "------------------------------------------------------------\n"
                +startingSkills + "\n"
                + "------------------------------------------------------------\n"
                +startingItems+ "\n"
                + "------------------------------------------------------------\n";

    }
}
