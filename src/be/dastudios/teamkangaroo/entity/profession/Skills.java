package be.dastudios.teamkangaroo.entity.profession;

public enum Skills {
    SHIELD_BASH,
    PIECING_SHOT,
    STEALTH,
    BACKSTAB
}
