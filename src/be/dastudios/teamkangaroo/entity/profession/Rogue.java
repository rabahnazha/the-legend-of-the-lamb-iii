package be.dastudios.teamkangaroo.entity.profession;

import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static be.dastudios.teamkangaroo.entity.Attributes.*;
import static be.dastudios.teamkangaroo.entity.Item.*;
import static be.dastudios.teamkangaroo.entity.profession.Skills.*;

public class Rogue extends Profession implements ProfessionFactory {
    private List<Skills> startingSkills;
    private final static int classBonusOfIntelligence = 1;
    private final static int classBonusOfConstitution = 0;
    private final static int classBonusOfDexterity = 3;
    private final static int healthPower = 5;

    {
        classBonus.put(DEXTERITY, classBonusOfDexterity);
        classBonus.put(INTELLIGENCE, classBonusOfIntelligence);
        initiativeBonus = 1;
    }

    public Rogue() {
        startingSkills = Stream.of(STEALTH, BACKSTAB).collect(Collectors.toList());
        startingItems = Stream.of(SHORT_STEEL_DAGGER, BLACK_HOODED_ROBES, X10_LOCKPICKS)
                .collect(Collectors.toList());
        healthPoints = (CONSTITUTION.getValue() + classBonusOfConstitution) * healthPower;
    }

    public List<Skills> getStartingSkills() {
        return startingSkills;
    }

    @Override
    public String getName() {
        return "rogue";
    }

        @Override
        public String toString() {
            String name = getName();
            String classBonusInfo = "Class Bonus : +"+classBonusOfDexterity+" Dexterity,+"+classBonusOfIntelligence+" Intelligence";
            String startingSkills = "Starting Skills : " + getStartingSkills().toString();
            String startingItems = "Starting Items:  " + super.startingItems.toString();
            String initiativeBonusInfo = "Initiative Bonus:  +" + initiativeBonus;

            return name + "\n"
                    + "------------------------------------------------------------\n"
                    +classBonusInfo + "\n"
                    + "------------------------------------------------------------\n"
                    +startingSkills + "\n"
                    + "------------------------------------------------------------\n"
                    +initiativeBonusInfo + "\n"
                    + "------------------------------------------------------------\n"
                    +startingItems+ "\n"
                    + "------------------------------------------------------------\n";


        }
}
