package be.dastudios.teamkangaroo.entity.profession.factory;


import be.dastudios.teamkangaroo.entity.Attributes;
import be.dastudios.teamkangaroo.entity.profession.Mage;
import be.dastudios.teamkangaroo.entity.profession.Ranger;
import be.dastudios.teamkangaroo.entity.profession.Rogue;
import be.dastudios.teamkangaroo.entity.profession.Warrior;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ProfessionFactoryClass {
    static Map<String, ProfessionFactory> professionMap= new HashMap<>();

    static {
        professionMap.put("mage", new Mage());
        professionMap.put("ranger", new Ranger());
        professionMap.put("rogue", new Rogue());
        professionMap.put("warrior", new Warrior());
    }

    public static Optional<ProfessionFactory> getProfession(String profession){
        return Optional.ofNullable(professionMap.get(profession));
    }


}
