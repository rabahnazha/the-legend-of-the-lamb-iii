package be.dastudios.teamkangaroo.entity.profession.factory;


import be.dastudios.teamkangaroo.entity.Attributes;
import be.dastudios.teamkangaroo.entity.Item;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface ProfessionFactory extends Serializable {
    String getName();
    Map<Attributes,Integer> getClassBonus() ;
    int getInitiativeBonus();
    int getHealthPoints();
    int getStaminaPoints();
    List<Item> getStartingItems();
}
