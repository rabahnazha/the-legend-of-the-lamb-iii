package be.dastudios.teamkangaroo.entity.enemy;

import java.io.Serializable;
import java.util.Random;

/**
 * Created By Moon
 * 2/22/2021, Mon
 **/
public enum TIER3 implements Enemy {
    VAMPIRE("Vampire",12,75,new Random().nextInt(15-10)+10),
    WARRIOR("Warrior",16,75,new Random().nextInt(15-10)+10),
    WITCH("Witch",14,75,new Random().nextInt(15-10)+10),
    ZOMBIE("Zombie",113,75,new Random().nextInt(15-10)+10);

    private String name;
    private int attackDamage;
    private int healthPoint;
    private int attributePoints;

    TIER3(String name, int attackDamage, int healthPoint, int attributePoints ) {
        this.name = name;
        this.attackDamage = attackDamage;
        this.healthPoint = healthPoint;
        this.attributePoints = attributePoints;
    }

    public int getAttributePoints() {
        return attributePoints;
    }

    public void setAttributePoints(int attributePoints) {
        this.attributePoints = attributePoints;
    }

    public String getName() {
        return name;
    }

    public int getAttackDamage() {
        return attackDamage;
    }


    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    @Override
    public String toString() {
        return
                "NAME OF ENEMY ='" + name + '\'' +
                        ", Attack Damage=" + attackDamage +
                        ", Health Point=" + healthPoint +
                        ", Attributes Points=" + attributePoints +
                        '}';
    }
}
