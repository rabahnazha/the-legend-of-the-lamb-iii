package be.dastudios.teamkangaroo.entity.enemy;


import java.io.Serializable;

public interface Enemy  {
    String getName();
    int getAttributePoints();
    int getHealthPoint();
    void setHealthPoint(int healtPoint);
    int getAttackDamage();

}
