package be.dastudios.teamkangaroo.entity.enemy;

import java.io.Serializable;
import java.util.Random;

/**
 * Created By Moon
 * 2/22/2021, Mon
 **/
public enum TIER4 implements Enemy {
    DRAGON("Dragon",22,100,new Random().nextInt(15-10)+10),
    GIANT("Giant",21,100,new Random().nextInt(15-10)+10),
    MAXIMUS("Maximus",23,100,new Random().nextInt(15-10)+10),
    HULK("Hulk",24,100,new Random().nextInt(15-10)+10);

    private String name;
    private int attackDamage;
    private int healthPoint;
    private int attributePoints;

    TIER4(String name, int attackDamage, int healthPoint, int attributePoints ) {
        this.name = name;
        this.attackDamage = attackDamage;
        this.healthPoint = healthPoint;
        this.attributePoints = attributePoints;
    }

    public int getAttributePoints() {
        return attributePoints;
    }

    public void setAttributePoints(int attributePoints) {
        this.attributePoints = attributePoints;
    }

    public String getName() {
        return name;
    }

    public int getAttackDamage() {
        return attackDamage;
    }


    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    @Override
    public String toString() {
        return
                "NAME OF ENEMY ='" + name + '\'' +
                        ", Attack Damage=" + attackDamage +
                        ", Health Point=" + healthPoint +
                        ", Attributes Points=" + attributePoints +
                        '}';
    }
}
