package be.dastudios.teamkangaroo.entity.enemy;


import java.io.Serializable;
import java.util.Random;

public enum TIER1 implements Enemy {
    BAT("bat",3,20,new Random().nextInt(15-10)+10),
    SLIME("slime",4,30,new Random().nextInt(15-10)+10),
    SNAKE("snake", 5,25,new Random().nextInt(15-10)+10),
    SPIDER("spider",4,20,new Random().nextInt(15-10)+10);

    private String name;
    private int attackDamage;
    private int healthPoint;
    private int attributePoints ;

    TIER1(String name, int attackDamage, int healthPoint,int attributePoints ) {
        this.name = name;
        this.attackDamage = attackDamage;
        this.healthPoint = healthPoint;
        this.attributePoints =attributePoints;
    }

    public int getAttributePoints() {
        return attributePoints;
    }

    public void setAttributePoints(int attributePoints) {
        this.attributePoints = attributePoints;
    }

    public String getName() {
        return name;
    }

    public int getAttackDamage() {
        return attackDamage;
    }



    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    @Override
    public String toString() {
        return
                "NAME OF ENEMY ='" + name + '\'' +
                ", Attack Damage=" + attackDamage +
                ", Health Point=" + healthPoint +
                 ", Attributes Points=" + attributePoints +
                '}';
    }


}
