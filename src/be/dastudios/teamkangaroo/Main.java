package be.dastudios.teamkangaroo;

import be.dastudios.teamkangaroo.constant.AppConstants;
import be.dastudios.teamkangaroo.entity.*;
import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactory;
import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactoryClass;
import be.dastudios.teamkangaroo.generator.RandomAdventurerGenerator;
import be.dastudios.teamkangaroo.service.ManagementService;
import be.dastudios.teamkangaroo.service.impl.ManagementServiceImpl;
import static be.dastudios.teamkangaroo.constant.AppConstants.*;
import static be.dastudios.teamkangaroo.utility.KeyboardUtility.ask;
import static be.dastudios.teamkangaroo.utility.KeyboardUtility.askForGender;

public class Main {

    public static void main(String[] args) {
    /*  Adventurer adventurer= RandomAdventurerGenerator.generateAdventurer();
        Inventory inventory = adventurer.getInventory();
        System.out.println(adventurer);
        System.out.println(inventory);
        inventory.addItem(Item.CASTLE_FORGED_ARMING_SWORD);
        System.out.println(adventurer);
        System.out.println(inventory);
        inventory.addItem(Item.CASTLE_FORGED_ARMING_SWORD);
        System.out.println(adventurer);
        System.out.println(inventory);
        inventory.addItem(Item.CASTLE_FORGED_ARMING_SWORD);
        System.out.println(adventurer);
        System.out.println(inventory);
        inventory.addItem(Item.CASTLE_FORGED_ARMING_SWORD);
        System.out.println(adventurer);
        System.out.println(inventory);*/



        ManagementService managementService = new ManagementServiceImpl() ;
        managementService.start();

    }
}
