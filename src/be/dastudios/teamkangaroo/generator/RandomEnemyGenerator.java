package be.dastudios.teamkangaroo.generator;

import be.dastudios.teamkangaroo.entity.Adventurer;
import be.dastudios.teamkangaroo.entity.enemy.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public  class RandomEnemyGenerator {
    private final static Random RAND_NUM_GEN = new Random();
    List<TIER1> listTier1 = new ArrayList<>();
    List<TIER2> listTier2 = new ArrayList<>();
    List<TIER3> listTier3 = new ArrayList<>();
    List<TIER4> listTier4 = new ArrayList<>();



    private static TIER1[] enemies1 = TIER1.values();
    private static TIER2[] enemies2 = TIER2.values();
    private static TIER3[] enemies3 = TIER3.values();
    private static TIER4[] enemies4 =TIER4.values();

    public static final TIER1 generateTier1(){
        return enemies1[RAND_NUM_GEN.nextInt(enemies1.length)];
    }
    public static final TIER2 generateTier2(){
        return enemies2[RAND_NUM_GEN.nextInt(enemies2.length)];
    }
    public static final TIER3 generateTier3(){
        return enemies3[RAND_NUM_GEN.nextInt(enemies3.length)];
    }
    public static final TIER4 generateTier4(){
        return enemies4[RAND_NUM_GEN.nextInt(enemies4.length)];
    }

    public List<TIER1> getListTier1() {
        return listTier1;
    }

    public void setListTier1(List<TIER1> listTier1) {
        this.listTier1 = listTier1;
    }

    public List<TIER2> getListTier2() {
        return listTier2;
    }

    public void setListTier2(List<TIER2> listTier2) {
        this.listTier2 = listTier2;
    }

    public List<TIER3> getListTier3() {
        return listTier3;
    }

    public void setListTier3(List<TIER3> listTier3) {
        this.listTier3 = listTier3;
    }

    public List<TIER4> getListTier4() {
        return listTier4;
    }

    public void setListTier4(List<TIER4> listTier4) {
        this.listTier4 = listTier4;
    }

    public void createTeam(int countOfTeamMembers) {
        for (int i = 0; i < countOfTeamMembers; i++) {
            listTier1.add(generateTier1());
            listTier2.add(generateTier2());
            listTier3.add(generateTier3());
            listTier4.add(generateTier4());
        }
    }

    public static  int generateRandomInitiativePoints(){
        return RAND_NUM_GEN.nextInt(8-2)+2;
    }


}
