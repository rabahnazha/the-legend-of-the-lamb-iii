package be.dastudios.teamkangaroo.generator;

import be.dastudios.teamkangaroo.entity.*;
import be.dastudios.teamkangaroo.entity.profession.Skills;
import be.dastudios.teamkangaroo.entity.profession.Spells;
import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactory;
import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactoryClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public  class RandomAdventurerGenerator {
    private final static Random RAND_NUM_GEN = new Random();
    private List<Adventurer> adventurerList = new ArrayList<>();

    private static Skills[] skills = Skills.values();
    private static Spells[] spells = Spells.values();
    private static Race[] races = Race.values();
    private static Item[] items = Item.values();
    private static Gender[] gender = Gender.values();
    private static Attributes[] attributes = Attributes.values();
    private static String[] professionNames = {"mage", "ranger", "rogue", "warrior" };


    public static final String generateProfessionName() {
        return professionNames[RAND_NUM_GEN.nextInt(professionNames.length)];
    }

    public static final Gender generateGender() {
        return gender[RAND_NUM_GEN.nextInt(gender.length)];
    }



    public static final Item generateItem() {
        return items[RAND_NUM_GEN.nextInt(items.length)];
    }

    public static final Race generateRace() {
        return races[RAND_NUM_GEN.nextInt(races.length)];
    }

    public static final Skills generateSkills() {
        return skills[RAND_NUM_GEN.nextInt(skills.length)];
    }


    public static final Spells generateSpells() {
        return spells[RAND_NUM_GEN.nextInt(spells.length)];
    }

    private static Inventory generateInventory(ProfessionFactory professionFactory) {
        List<Item> itemList = professionFactory.getStartingItems();
        Inventory inventory = new Inventory(itemList);
        return inventory;

    }

    public static final Adventurer generateAdventurer(){
        int order = RAND_NUM_GEN.nextInt(1000);
        String name= "Buddyy_" + order;
        Gender gender = generateGender();
        String myProfessionName = generateProfessionName();
        ProfessionFactory myProfession = ProfessionFactoryClass.getProfession(myProfessionName).orElseThrow(() ->new IllegalArgumentException("Invalid Profession"));
        Inventory inventory = generateInventory(myProfession);
        Race race = generateRace();
        Adventurer adventurer = new Adventurer(name,gender);
        adventurer.setRace(race);
        adventurer.setInventory(inventory);
        adventurer.setProfessionFactory(myProfession);

        return adventurer;

    }

    public List<Adventurer> getAdventurerList() {
        return adventurerList;
    }

    public void setAdventurerList(List<Adventurer> adventurerList) {
        this.adventurerList = adventurerList;
    }

    public List<Adventurer> createTeam(int countOfTeamMembers) {
        for (int i = 0; i < countOfTeamMembers; i++) {
            adventurerList.add(generateAdventurer());
        }
        return adventurerList;
    }
}