package be.dastudios.teamkangaroo.generator;

import be.dastudios.teamkangaroo.entity.*;
import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactory;
import be.dastudios.teamkangaroo.entity.profession.factory.ProfessionFactoryClass;

import java.util.List;
import java.util.Random;

import static be.dastudios.teamkangaroo.entity.Gender.*;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public  class AdventurerGenerator {
    private final  Random RAND_NUM_GEN = new Random();
    private  String name;
    private  String genderName;
    private  String raceName;
    private  String professionName;


    private  String[] professionNames = {"mage", "ranger", "rogue", "warrior"};

    public AdventurerGenerator(String name, String genderName, String raceName, String professionName) {
        this.name = name;
        this.genderName = genderName;
        this.raceName = raceName;
        this.professionName = professionName;
    }

    public  Gender generateGender(String genderName) {
        if (genderName.toUpperCase().equals(MALE.toString())) return MALE;
        else return FEMALE;
    }


    public  Race generateRace(String raceName) {
        if (raceName.toUpperCase().equals("HUMAN")) return Race.HUMAN;
        else if (raceName.toUpperCase().equals("ELF")) return Race.ELF;
        else return Race.DWARF;
    }

    public  ProfessionFactory generateProfession(String professionName) {

        ProfessionFactory professionFactory = ProfessionFactoryClass.getProfession(professionName)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Profession"));
        return professionFactory;
    }


    public  Adventurer generateAdventurer() {
        Gender gender = generateGender(genderName);
        ProfessionFactory professionFactory=generateProfession(professionName);
        Inventory inventory = generateInventory(professionFactory);
        Race race = generateRace(raceName);
        Adventurer adventurer = new Adventurer(name, gender);
        adventurer.setRace(race);
        adventurer.setInventory(inventory);
        adventurer.setProfessionFactory(professionFactory);
        return adventurer;

    }

    private Inventory generateInventory(ProfessionFactory professionFactory) {
        List<Item> itemList = professionFactory.getStartingItems();
        Inventory inventory = new Inventory(itemList);
        return inventory;

    }
}